import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { Article } from 'src/app/interfaces';
import { NewsService } from 'src/app/services/news.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page implements OnInit {
  @ViewChild(IonInfiniteScroll, { static: true })
  infinitiScroll!: IonInfiniteScroll;

  public articles: Article[] = [];
  public categories: string[] = [
    'business',
    'entertainment',
    'general',
    'health',
    'science',
    'sports',
    'technology',
  ];

  public selecteCategory: string = this.categories[0];
  constructor(private newServicio: NewsService) {}

  ngOnInit(): void {
    console.log(this.infinitiScroll);
    this.servicio();
  }
  segmentChanged(category: Event) {
    this.selecteCategory = (category as CustomEvent).detail.value;
    this.servicio();
  }
  servicio() {
    this.newServicio
      .getTopHeadLinesByCategory(this.selecteCategory)
      .subscribe((resp) => (this.articles = [...resp]));
  }

  loadData() {
    this.newServicio
      .getTopHeadLinesByCategory(this.selecteCategory, true)
      .subscribe((resp) => {
        if (resp.length === this.articles.length) {
          this.infinitiScroll.disabled = true;

          return;
        }
        this.articles = resp;

        setTimeout(() => {
          this.infinitiScroll.complete();
          //event.target.complete();
        }, 1000);
      });
  }
}
