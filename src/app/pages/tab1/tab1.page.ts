import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { Article } from 'src/app/interfaces';
import { NewsService } from 'src/app/services/news.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page implements OnInit {
  articles: Article[] = [];

  @ViewChild(IonInfiniteScroll, { static: true })
  infinitiScroll!: IonInfiniteScroll;
  constructor(private newService: NewsService) {}
  ngOnInit(): void {
    this.newService.getTopHeadLines().subscribe((articles) => {
      //this.article.push(...articles)
      this.articles = [...this.articles, ...articles];
    });
  }

  loadData() {
    this.newService
      .getTopHeadLinesByCategory('business', true)
      .subscribe((resp) => {
        if (resp.length === this.articles.length) {
          this.infinitiScroll.disabled = true;

          return;
        }
        this.articles = resp;

        setTimeout(() => {
          this.infinitiScroll.complete();
          //event.target.complete();
        }, 1000);
      });
  }
}
