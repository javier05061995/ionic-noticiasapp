import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import {
  Article,
  ArticlesByCategoryAndPage,
  RespuestaTopHeadlines,
} from '../interfaces';
import { Observable } from 'rxjs';

import { map, of } from 'rxjs';

const apiKey = environment.apiKey;
const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root',
})
export class NewsService {
  private articlesByCategoryAndPage: ArticlesByCategoryAndPage = {};

  constructor(private http: HttpClient) {}

  private executeQuery<J>(endPoint: string) {
    console.log('peticion a la api');

    return this.http.get<J>(`${apiUrl}${endPoint}`, {
      params: {
        apiKey: apiKey,
        country: 'us',
      },
    });
  }

  getTopHeadLines(): Observable<Article[]> {
    return this.getTopHeadLinesByCategory('business');
    // return this.executeQuery<RespuestaTopHeadlines>(
    //   `/top-headlines?category=business`
    // ).pipe(map(({ articles }) => articles));
  }

  getTopHeadLinesByCategory(
    category: string,
    loadMore: boolean = false
  ): Observable<Article[]> {
    if (loadMore) {
      return this.getArticleByCategory(category);
    }

    if (this.articlesByCategoryAndPage[category]) {
      return of(this.articlesByCategoryAndPage[category].articles);
    }

    return this.getArticleByCategory(category);
  }

  private getArticleByCategory(category: string): Observable<Article[]> {
    if (!Object.keys(this.articlesByCategoryAndPage).includes(category)) {
      //this.articlesByCategoryAndPage[category].page += 1;
      this.articlesByCategoryAndPage[category] = {
        page: 0,
        articles: [],
      };
    }
    const page = this.articlesByCategoryAndPage[category].page + 1;

    return this.executeQuery<RespuestaTopHeadlines>(
      `/top-headlines?category=${category}&page=${page}`
    ).pipe(
      map(({ articles }) => {
        if (articles.length === 0)
          return this.articlesByCategoryAndPage[category].articles;

        this.articlesByCategoryAndPage[category] = {
          page: page,
          articles: [
            ...this.articlesByCategoryAndPage[category].articles,
            ...articles,
          ],
        };

        return this.articlesByCategoryAndPage[category].articles;
      })
    );
  }
}
