import { Component, Input } from '@angular/core';
import { Article } from '../../interfaces/index';
import { Browser } from '@capacitor/browser';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss'],
})
export class ArticleComponent {
  @Input() article!: Article;
  @Input() index!: number;

  constructor(
    // private iab: InAppBrowser,
    private plataform: Platform
  ) {}

  openArticle() {
    if (this.plataform.is('ios') || this.plataform.is('android')) {
    }
    Browser.open({ url: this.article.url });
  }
}
